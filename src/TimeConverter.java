import java.util.Scanner;

import java.util.Scanner;

public class TimeConverter {
    public static void showTimeConverter()
    {
        Scanner scanner = new Scanner(System.in);
        double input_value;

        boolean running = true;
        while (running) {
            System.out.print("""
                    ============================
                         - Конвертор часу -
                           
                    [s] Конвертувати в секунди
                    [m] Конвертувати в хвилини
                    [h] Конвертувати в години
                    
                    [Q] Вихід
                    
                    """);

            System.out.println("Оберіть функцію: ");
            String operation = scanner.next();
            if (operation.equals("Q")) {
                running = false;
                continue;
            }
            else if (!isTimeMeasure(operation)) {
                System.out.print(Color.RED + "\nТакої часової міри не існує, спробуйте знову\n\n" + Color.RESET);
                continue;
            }

            System.out.println("Оберіть вхідну міру часу: ");
            String in_measure = scanner.next();
            if (!isTimeMeasure(in_measure)) {
                System.out.print(Color.RED + "\nТакої часової міри не існує, спробуйте знову\n\n" + Color.RESET);
                continue;
            }

            if (operation.equals(in_measure)) {
                System.out.print(Color.YELLOW + "\nКонвертуйте різні часові міри\n\n" + Color.RESET);
                continue;
            }

            System.out.println("Введіть кількість часу: ");
            try {
                input_value = scanner.nextDouble();
                if (input_value < 0) {
                    throw new ArithmeticException();
                }
                double result = convertTime(in_measure, operation, input_value);
                System.out.print("\nРезультат: " + Color.GREEN + result + Color.RESET + "\n\n");
            }
            catch (java.util.InputMismatchException e) {
                System.out.print(Color.RED + "\nПомилка вводу числа, спробуйте знову\n\n" + Color.RESET);
                scanner.nextLine();
            }
            catch (ArithmeticException e) {
                System.out.print(Color.RED + "\nЧас не може бути менше нуля\n\n" + Color.RESET);
                scanner.nextLine();
            }
        }
    }

    public static double convertTime (String in_measure, String out_measure, double input_value) {
        double result = 0;
        switch (in_measure) {
            case "s":
                if (out_measure.equals("m")) {
                    result = input_value * 1/60;
                }
                else if (out_measure.equals("h")) {
                    result = input_value * 1/(60*60);
                }
                break;
            case "m":
                if (out_measure.equals("s")) {
                    result = input_value * 60;
                }
                else if (out_measure.equals("h")) {
                    result = input_value * 1/60;
                }
                break;
            case "h":
                if (out_measure.equals("s")) {
                    result = input_value*60*60;
                }
                else if (out_measure.equals("m")) {
                    result = input_value*60;
                }
                break;
            default:
                break;
        }
        return result;
    }

    private static boolean isTimeMeasure(String string) {
        return string.equals("s") || string.equals("m") || string.equals("h");
    }
}

