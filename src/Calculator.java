import java.util.Scanner;

public class Calculator {
    private static float num;
    private static float result;

    public static void showMenu () {

        Scanner scanner = new Scanner(System.in);

        boolean running = true;
        while (running) {

            System.out.printf("""
                    =============================
                           - Калькулятор -
                           
                    Результат: %s
                        
                    +   -   *   /   time   C   Q   area
                    Оберіть функцію:
                    """, result);

            String operation = scanner.next();

            if (operation.equals("Q")) {
                running = false;
            }
            else if (operation.equals("time")) {
                TimeConverter.showTimeConverter();
            }
            else if (operation.equals("area")) {
                AreaConverter.run();
            }
            else if (operation.equals("C")) {
                result = 0;
            }
            else if (isMathOperation(operation)) {
                System.out.println("Введіть число: ");
                try {
                    num = scanner.nextFloat();
                    calculateExpression(operation);
                }
                catch (java.util.InputMismatchException e) {
                    System.out.print(Color.RED + "\nПомилка вводу числа, спробуйте знову.\n\n" + Color.RESET);
                    scanner.nextLine();
                }
            }
            else {
                System.out.print(Color.RED + "\nНевідома операція, спробуйте знову.\n\n" + Color.RESET);
            }
        }
    }

    public static void calculateExpression (String operation) {
        switch (operation) {
            case "+":
                result += num;
                break;
            case "-":
                result -= num;
                break;
            case "*":
                result *= num;
                break;
            case "/":
                if (num == 0) {
                    System.out.print(Color.RED + "\nПомилка: ділення на нуль\n\n" + Color.RESET);
                    break;
                }
                result /= num;
                break;
            default:
                break;
        }
    }

    private static boolean isMathOperation(String string) {
        return string.equals("+") || string.equals("-") || string.equals("*") || string.equals("/");
    }
}
