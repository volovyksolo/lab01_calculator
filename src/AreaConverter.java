//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

import java.util.Scanner;

public class AreaConverter {
    protected static int digitsAfterComma = 10;

    public AreaConverter() {
    }

    protected static double squareMetersToSquareCentimeters(double squareMeters) {
        return roundValue(squareMeters * 10000.0);
    }

    protected static double squareMetersToSquareMillimeters(double squareMeters) {
        return roundValue(squareMeters * 1000000.0);
    }

    protected static double squareCentimetersToSquareMeters(double squareCentimeters) {
        return roundValue(squareCentimeters / 10000.0);
    }

    protected static double squareCentimetersToSquareMillimeters(double squareCentimeters) {
        return roundValue(squareCentimeters * 100.0);
    }

    protected static double squareMillimetersToSquareMeters(double squareMillimeters) {
        return roundValue(squareMillimeters / 1000000.0);
    }

    protected static double squareMillimetersToSquareCentimeters(double squareMillimeters) {
        return roundValue(squareMillimeters / 100.0);
    }

    protected static double roundValue(double val) {
        double divisor = Math.pow(10.0, (double)digitsAfterComma);
        return (double)Math.round(val * divisor) / divisor;
    }

    protected static void displayAreaConverterContextMenu() {
        System.out.println("Оберiть операцiю:");
        System.out.println("1. Метр^2 до Сантиметра^2");
        System.out.println("2. Метр^2 до Мiлiметра^2");
        System.out.println("3. Сантиметр^2 до Метра^2");
        System.out.println("4. Сантиметр^2 до Мiлiметра^2");
        System.out.println("5. Мiлiметр^2 до Метра^2");
        System.out.println("6. Мiлiметр^2 до Сантиметр^2");
        System.out.println("7. Вийти з калькулятора площини");
    }

    public static void run() {
        Scanner scanner = new Scanner(System.in);

        double result = 0.0;

        while(true) {
            displayAreaConverterContextMenu();
            int option = scanner.nextInt();
            if (option == 7) {
                return;
            }

            if (option < 1 || option > 7) {
                throw new IllegalArgumentException(Color.RED + "Такої операцiї не iснує!" + Color.RESET);
            }

            System.out.print("Введiть число: ");
            double val = scanner.nextDouble();
            switch (option) {
                case 1:
                    result = squareMetersToSquareCentimeters(val);
                    break;
                case 2:
                    result = squareMetersToSquareMillimeters(val);
                    break;
                case 3:
                    result = squareCentimetersToSquareMeters(val);
                    break;
                case 4:
                    result = squareCentimetersToSquareMillimeters(val);
                    break;
                case 5:
                    result = squareMillimetersToSquareMeters(val);
                    break;
                case 6:
                    result = squareMillimetersToSquareCentimeters(val);
            }

            System.out.println("Результат: " + result);
        }
    }
}
