# Дослідження системи контролю версій GIT та налагодження оточення Java/Kotlin розробника

## Мета.

Навчитися налагоджувати оточення Java/Kotlin-розробника. Вивчити основи використання системи GIT.

## Завдання 0 (обов’язкове)

1. Встановити GIT: https://gitforwindows.org/<br/>
2. Ознайомитися з розділами книги 1-6 Pro-Git:<br/>
   Главы 1-6 https://git-scm.com/book/uk/v2<br/>
3. Пройти тренінг: http://learngitbranching.js.org/. Зробити скріншоти.

   <p align="center">![Рис. 1](img/learn-git-branching1.png)</p>
   <p align="center">Рис. 1. <b>Результат виконання тренінгу з Git (1)</b></p>
   <br/><br/>

   <p align="center">![Рис. 2](img/learn-git-branching2.jpg)</p> 
   <p align="center">Рис. 2. <b>Результат виконання тренінгу з Git (2)</b></p>

   <p>Виконання окремих завдань тренінгу я надав у кінці цього файлу</p>

## Завдання 2 (командне 75-100%)

#### 2.1

<p>Інсталювати Open JDK 17 (LTS) https://adoptium.net/?variant=openjdk17&jvmVariant=hotspot<br/></p>
<p>Інсталювати Idea Jetbrains. https://www.jetbrains.com/idea/download/download-thanks.html?platform=windows</p>

#### 2.2

<p>Для реалізації консольного калькулятора з основними функціями (+, -, /, \*) і одним індивідуальним завданням дивіться таблицю.</p>

| Номер варіанту | Вираз                                       |
| -------------- | ------------------------------------------- |
| 6              | Перетворення часу (мінута, година, секунда) |

<p>При реалізації калькулятора необхідно використовувати систему
контролю версій. Застосувати стратегію розгалуження, гілку до
функціональності (ознака-гілка). Кожен коміт повинен містити
мінімальну кількість змін, що дозволяють реалізувати частину або
частину завдання. Під кожним функціоналом використовується
окрема гілка.
Вивчити підходи до організації структури філій в ГІТ-репозитарії.</p>
<br/><br/>

1. Основна частина:

   <p align="center">![Рис. 3](img/1.jpg)</p> 
   <p align="center">Рис. 3. <b>Створення нового репозитарію</b></p>
   <br/><br/>

   <p align="center">![Рис. 4](img/2.jpg)</p> 
   <p align="center">Рис. 4. <b>Налаштування .gitignore</b></p>
   <br/><br/>

   <p align="center">![Рис. 5](img/3.jpg)</p> 
   <p align="center">Рис. 5. <b>Клонування віддаленого репозитарію в IDEA</b></p>
   <br/><br/>

   <p align="center">![Рис. 6](img/4.jpg)</p> 
   <p align="center">Рис. 6. <b>Встановлення локальних налаштувань для ідентифікації користувача</b></p>
   <br/><br/>

   <p align="center">![Рис. 7](img/5.png)</p> 
   <p align="center">Рис. 7. <b>Ініціалізація проєкту</b></p>
   <br/><br/>

   <p align="center">![Рис. 8](img/6.jpg)</p> 
   <p align="center">Рис. 8. <b>Написання першої логіки програми</b></p>
   <br/><br/>

   <p align="center">![Рис. 9](img/7.jpg)</p> 
   <p align="center">Рис. 9. <b>Створення гілки для розробки функціоналу калькулятора</b></p>
   <br/><br/>

   <p align="center">![Рис. 10](img/8.jpg)</p> 
   <p align="center">Рис. 10. <b>Результат створення гілки</b></p>
   <br/><br/>

   <p align="center">![Рис. 11](img/9.jpg)</p> 
   <p align="center">Рис. 11. <b>Робота з коментарями TODO</b></p>
   <br/><br/>

   <p align="center">![Рис. 12](img/10.jpg)</p> 
   <p align="center">Рис. 12. <b>Коммітування результату розробки обгортки калькулятора</b></p>
   <br/><br/>

   <p align="center">![Рис. 13](img/11.jpg)</p> 
   <p align="center">Рис. 13. <b>Коммітування валідатора математичних операторів</b></p>
   <br/><br/>

   <p align="center">![Рис. 14](img/12.jpg)</p> 
   <p align="center">Рис. 14. <b>Коммітування калькулятора з перевіркою доданих файлів</b></p>
   <br/><br/>

   <p align="center">![Рис. 15](img/13.jpg)</p> 
   <p align="center">Рис. 15. <b>Створення + перехід на нову гілку для розробки ще однієї фічі</b></p>
   <br/><br/>

   <p align="center">![Рис. 16](img/14.jpg)</p> 
   <p align="center">Рис. 16. <b>Процес розробки фічі - конвертора часу</b></p>
   <br/><br/>

   <p align="center">![Рис. 17](img/15.jpg)</p>
   <p align="center">Рис. 17. <b>Коммітування конвертора часу</b></p>
   <br/><br/>

   <p align="center">![Рис. 18](img/17.jpg)</p>
   <p align="center">Рис. 18. <b>Коммітування окремої функції конвертора</b></p>
   <br/><br/>

   <p align="center">![Рис. 19](img/18.jpg)</p>
   <p align="center">Рис. 19. <b>Завершення розробки логіки конвертора</b></p>
   <br/><br/>

   <p align="center">![Рис. 20](img/19.jpg)</p>
   <p align="center">Рис. 20. <b>Робота з классом кольорів для позначення повідомлень різних типів (використовується в інших классах) + злиття з гілкою конвертора часу</b></p>
   <br/><br/>

   <p align="center">![Рис. 21](img/20.jpg)</p>
   <p align="center">Рис. 21. <b>Доопрацювання раніше розроблених классів кольоровими повідомленнями</b></p>
   <br/><br/>

   <p align="center">![Рис. 22](img/21.jpg)</p>
   <p align="center">Рис. 22. <b>Виправлення багів перед злиттям з Main</b></p>
   <br/><br/>

   <p align="center">![Рис. 23](img/22.jpg)</p>
   <p align="center">Рис. 23. <b>Нарешті, коли всі побічні гілки об'єднані, зливаємо з Main</b></p>
   <br/><br/>

   <p align="center">![Рис. 24](img/24.jpg)</p>
   <p align="center">Рис. 24. <b>Push усіх гілок у віддалений репозитарій</b></p>
   <br/><br/>

   <p align="center">![Рис. 25](img/25.jpg)</p>
   <p align="center">Рис. 25. <b>Дерево git</b></p>
   <br/><br/>

   <p align="center">![Рис. 26](img/26.jpg)</p>
   <p align="center">Рис. 26. <b>Перейменування віддаленої гілки (я перейменував усі гілки для зрозумілості)</b></p>
   <br/><br/>

   <p align="center">![Рис. 27](img/27.jpg)</p>
   <p align="center">Рис. 27. <b>Перегляд змін в комітах на GitLabs</b></p>
   <br/><br/>

2. Командна частина:

   <p>Об'єднуйтеся в команди по дві людини. Додайте один одного до
   своїх репозиторіїв GitLab. Реалізуйте свою додаткову функцію для
   цього проекту.</p>

   <p align="center">![Рис. 28](img/32.jpg)</p>
   <p align="center">Рис. 28. <b>Додавання учасника на проєкт</b></p>
   <br/><br/>

   <p align="center">![Рис. 29](img/28.jpg)</p>
   <p align="center">Рис. 29. <b>Коміт від учасника по команді + запит на merge</b></p>
   <br/><br/>

   <p align="center">![Рис. 30](img/29.jpg)</p>
   <p align="center">Рис. 30. <b>Зміст коміту - реалізація логіки конвертера площі та впровадження його в існуючу систему</b></p>
   <br/><br/>

   <p align="center">![Рис. 31](img/35.jpg)</p>
   <p align="center">Рис. 31. <b>Єгор наявний в історії, але він забув встановити локальне ім'я</b></p>
   <br/><br/>

   <p align="center">![Рис. 32](img/30.jpg)</p>
   <p align="center">Рис. 32. <b>Завантаження напрацювань товариша з віддаленого репозитарію у локальний</b></p>
   <br/><br/>

   <p align="center">![Рис. 33](img/31.jpg)</p>
   <p align="center">Рис. 33. <b>Функція від іншого розробника успішно додана!</b></p>
   <br/><br/>

   <p align="center">![Рис. 34](img/33.png)</p>
   <p align="center">Рис. 34. <b>Запит на додавання моєї функції до проєкту напарника</b></p>
   <br/><br/>

   <p align="center">![Рис. 35](img/34.jpg)</p>
   <p align="center">Рис. 35. <b>Встановлення конвертора часу в історії проєкту Єгора (теж не налаштовано ім'я через git config --local)</b></p>
   <br/><br/>

## Тренінг:

1. Основи:
   <p align="center">![Рис. 36.1](img/course/1_1.jpg)</p>
   <p align="center">Рис. 36.1. <b>Умова завдання 1</b></p>
   <br/><br/>
   <p align="center">![Рис. 36.2](img/solutions/1.jpg)</p>
   <p align="center">Рис. 36.2. <b>Результат виконання завдання 1 (1)</b></p>
   <br/><br/>
   <p align="center">![Рис. 36.3](img/course/1_3.jpg)</p>
   <p align="center">Рис. 36.3. <b>Результат виконання завдання 1 (2)</b></p>
   <br/><br/>

   <p align="center">![Рис. 37.1](img/course/2_1.jpg)</p>
   <p align="center">Рис. 37.1. <b>Умова завдання 2</b></p>
   <br/><br/>
   <p align="center">![Рис. 37.2](img/solutions/2.jpg)</p>
   <p align="center">Рис. 37.2. <b>Результат виконання завдання 2 (1)</b></p>
   <br/><br/>
   <p align="center">![Рис. 37.3](img/course/2_3.jpg)</p>
   <p align="center">Рис. 37.3. <b>Результат виконання завдання 2 (2)</b></p>
   <br/><br/>

   <p align="center">![Рис. 38.1](img/course/3_1.jpg)</p>
   <p align="center">Рис. 38.1. <b>Умова завдання 3</b></p>
   <br/><br/>
   <p align="center">![Рис. 38.2](img/solutions/3.jpg)</p>
   <p align="center">Рис. 38.2. <b>Результат виконання завдання 3 (1)</b></p>
   <br/><br/>
   <p align="center">![Рис. 38.3](img/course/3_3.jpg)</p>
   <p align="center">Рис. 38.3. <b>Результат виконання завдання 3 (2)</b></p>
   <br/><br/>

   <p align="center">![Рис. 39.1](img/course/4_1.jpg)</p>
   <p align="center">Рис. 39.1. <b>Умова завдання 4</b></p>
   <br/><br/>
   <p align="center">![Рис. 39.2](img/solutions/4.jpg)</p>
   <p align="center">Рис. 39.2. <b>Результат виконання завдання 4 (1)</b></p>
   <br/><br/>
   <p align="center">![Рис. 39.3](img/course/4_3.jpg)</p>
   <p align="center">Рис. 39.3. <b>Результат виконання завдання 4 (2)</b></p>
   <br/><br/>

   <p align="center">![Рис. 40.1](img/course/5_1.jpg)</p>
   <p align="center">Рис. 40.1. <b>Умова завдання 5</b></p>
   <br/><br/>
   <p align="center">![Рис. 40.2](img/solutions/5.jpg)</p>
   <p align="center">Рис. 40.2. <b>Результат виконання завдання 5 (1)</b></p>
   <br/><br/>
   <p align="center">![Рис. 40.3](img/course/5_3.jpg)</p>
   <p align="center">Рис. 40.3. <b>Результат виконання завдання 5 (2)</b></p>
   <br/><br/>

   <p align="center">![Рис. 41.1](img/course/6_1.jpg)</p>
   <p align="center">Рис. 41.1. <b>Умова завдання 6</b></p>
   <br/><br/>
   <p align="center">![Рис. 41.2](img/solutions/6.jpg)</p>
   <p align="center">Рис. 41.2. <b>Результат виконання завдання 6 (1)</b></p>
   <br/><br/>
   <p align="center">![Рис. 41.3](img/course/6_3.jpg)</p>
   <p align="center">Рис. 41.3. <b>Результат виконання завдання 6 (2)</b></p>
   <br/><br/>

   <p align="center">![Рис. 42.1](img/course/7_1.jpg)</p>
   <p align="center">Рис. 42.1. <b>Умова завдання 7</b></p>
   <br/><br/>
   <p align="center">![Рис. 36](img/solutions/7.jpg)</p>
   <p align="center">Рис. 42.2. <b>Результат виконання завдання 7 (1)</b></p>
   <br/><br/>
   <p align="center">![Рис. 36](img/course/7_3.jpg)</p>
   <p align="center">Рис. 42.3. <b>Результат виконання завдання 7 (2)</b></p>
   <br/><br/>

   <p align="center">![Рис. 43.1](img/course/8_1.jpg)</p>
   <p align="center">Рис. 43.1. <b>Умова завдання 8</b></p>
   <br/><br/>
   <p align="center">![Рис. 43.2](img/solutions/8.jpg)</p>
   <p align="center">Рис. 43.2. <b>Результат виконання завдання 8 (1)</b></p>
   <br/><br/>
   <p align="center">![Рис. 43.3](img/course/8_3.jpg)</p>
   <p align="center">Рис. 43.3. <b>Результат виконання завдання 8 (2)</b></p>
   <br/><br/>

   <p align="center">![Рис. 44.1](img/course/9_1.jpg)</p>
   <p align="center">Рис. 44.1. <b>Умова завдання 9</b></p>
   <br/><br/>
   <p align="center">![Рис. 44.2](img/solutions/9.jpg)</p>
   <p align="center">Рис. 44.2. <b>Результат виконання завдання 9 (1)</b></p>
   <br/><br/>
   <p align="center">![Рис. 44.3](img/course/9_3.jpg)</p>
   <p align="center">Рис. 44.3. <b>Результат виконання завдання 9 (2)</b></p>
   <br/><br/>

   <p align="center">![Рис. 45.1](img/course/10_1.jpg)</p>
   <p align="center">Рис. 45.1. <b>Умова завдання 10</b></p>
   <br/><br/>
   <p align="center">![Рис. 45.2](img/solutions/10.jpg)</p>
   <p align="center">Рис. 45.2. <b>Результат виконання завдання 10 (1)</b></p>
   <br/><br/>
   <p align="center">![Рис. 45.3](img/course/10_3.jpg)</p>
   <p align="center">Рис. 45.3. <b>Результат виконання завдання 10 (2)</b></p>
   <br/><br/>
   <p align="center">![Рис. 45.4](img/course/10_4.jpg)</p>
   <p align="center">Рис. 45.4. <b>Результат виконання завдання 10 (3)</b></p>
   <br/><br/>

   <p align="center">![Рис. 46.1](img/course/11_1.jpg)</p>
   <p align="center">Рис. 46.1. <b>Умова завдання 11</b></p>
   <br/><br/>
   <p align="center">![Рис. 46.2](img/solutions/11.jpg)</p>
   <p align="center">Рис. 46.2. <b>Результат виконання завдання 11 (1)</b></p>
   <br/><br/>
   <p align="center">![Рис. 46.3](img/course/11_3.jpg)</p>
   <p align="center">Рис. 46.3. <b>Результат виконання завдання 11 (2)</b></p>
   <br/><br/>

   <p align="center">![Рис. 47.1](img/course/12_1.jpg)</p>
   <p align="center">Рис. 47.1. <b>Умова завдання 12</b></p>
   <br/><br/>
   <p align="center">![Рис. 47.2](img/solutions/12.jpg)</p>
   <p align="center">Рис. 47.2. <b>Результат виконання завдання 12 (1)</b></p>
   <br/><br/>
   <p align="center">![Рис. 47.3](img/course/12_3.jpg)</p>
   <p align="center">Рис. 47.3. <b>Результат виконання завдання 12 (2)</b></p>
   <br/><br/>
   <p align="center">![Рис. 47.4](img/course/12_4.jpg)</p>
   <p align="center">Рис. 47.4. <b>Результат виконання завдання 12 (3)</b></p>
   <br/><br/>
   <p align="center">![Рис. 47.5](img/course/12_5.jpg)</p>
   <p align="center">Рис. 47.5. <b>Результат виконання завдання 12 (4)</b></p>
   <br/><br/>

   <p align="center">![Рис. 48.1](img/course/13_1.jpg)</p>
   <p align="center">Рис. 48.1. <b>Умова завдання 13</b></p>
   <br/><br/>
   <p align="center">![Рис. 48.2](img/solutions/13.jpg)</p>
   <p align="center">Рис. 48.2. <b>Результат виконання завдання 13 (1)</b></p>
   <br/><br/>
   <p align="center">![Рис. 48.3](img/course/13_3.jpg)</p>
   <p align="center">Рис. 48.3. <b>Результат виконання завдання 13 (2)</b></p>
   <br/><br/>

   <p align="center">![Рис. 49.1](img/course/14_1.jpg)</p>
   <p align="center">Рис. 49.1. <b>Умова завдання 14</b></p>
   <br/><br/>
   <p align="center">![Рис. 49.2](img/solutions/14.jpg)</p>
   <p align="center">Рис. 49.2. <b>Результат виконання завдання 14 (1)</b></p>
   <br/><br/>
   <p align="center">![Рис. 49.3](img/course/14_3.jpg)</p>
   <p align="center">Рис. 49.3. <b>Результат виконання завдання 14 (2)</b></p>
   <br/><br/>

   <p align="center">![Рис. 50.1](img/course/15_1.jpg)</p>
   <p align="center">Рис. 50.1. <b>Умова завдання 15</b></p>
   <br/><br/>
   <p align="center">![Рис. 50.2](img/solutions/15.jpg)</p>
   <p align="center">Рис. 50.2. <b>Результат виконання завдання 15 (1)</b></p>
   <br/><br/>
   <p align="center">![Рис. 50.3](img/course/15_3.jpg)</p>
   <p align="center">Рис. 50.3. <b>Результат виконання завдання 15 (2)</b></p>
   <br/><br/>

   <p align="center">![Рис. 51.1](img/course/16_1.jpg)</p>
   <p align="center">Рис. 51.1. <b>Умова завдання 16</b></p>
   <br/><br/>
   <p align="center">![Рис. 51.2](img/solutions/16.jpg)</p>
   <p align="center">Рис. 51.2. <b>Результат виконання завдання 16 (1)</b></p>
   <br/><br/>
   <p align="center">![Рис. 51.3](img/course/16_3.jpg)</p>
   <p align="center">Рис. 51.3. <b>Результат виконання завдання 16 (2)</b></p>
   <br/><br/>
   <p align="center">![Рис. 51.4](img/course/16_4.jpg)</p>
   <p align="center">Рис. 51.4. <b>Результат виконання завдання 16 (3)</b></p>
   <br/><br/>

   <p align="center">![Рис. 52.1](img/course/17_1.jpg)</p>
   <p align="center">Рис. 52.1. <b>Умова завдання 17</b></p>
   <br/><br/>
   <p align="center">![Рис. 52.2](img/solutions/17.jpg)</p>
   <p align="center">Рис. 52.2. <b>Результат виконання завдання 17 (1)</b></p>
   <br/><br/>
   <p align="center">![Рис. 52.3](img/course/17_3.jpg)</p>
   <p align="center">Рис. 52.3. <b>Результат виконання завдання 17 (2)</b></p>
   <br/><br/>

   <p align="center">![Рис. 53.1](img/course/18_1.jpg)</p>
   <p align="center">Рис. 53.1. <b>Умова завдання 18</b></p>
   <br/><br/>
   <p align="center">![Рис. 53.2](img/solutions/18.jpg)</p>
   <p align="center">Рис. 53.2. <b>Результат виконання завдання 18 (1)</b></p>
   <br/><br/>
   <p align="center">![Рис. 53.3](img/course/18_3.jpg)</p>
   <p align="center">Рис. 53.3. <b>Результат виконання завдання 18 (2)</b></p>
   <br/><br/>

2. Віддалені репозитарії:
   <p align="center">![Рис. 54.1](img/course/19_1.jpg)</p>
   <p align="center">Рис. 54.1. <b>Умова завдання 19</b></p>
   <br/><br/>
   <p align="center">![Рис. 54.2](img/solutions/19.jpg)</p>
   <p align="center">Рис. 54.2. <b>Результат виконання завдання 19 (1)</b></p>
   <br/><br/>
   <p align="center">![Рис. 54.3](img/course/19_3.jpg)</p>
   <p align="center">Рис. 54.3. <b>Результат виконання завдання 19 (2)</b></p>
   <br/><br/>

   <p align="center">![Рис. 55.1](img/course/20_1.jpg)</p>
   <p align="center">Рис. 55.1. <b>Умова завдання 20</b></p>
   <br/><br/>
   <p align="center">![Рис. 55.2](img/solutions/20.jpg)</p>
   <p align="center">Рис. 55.2. <b>Результат виконання завдання 20 (1)</b></p>
   <br/><br/>
   <p align="center">![Рис. 55.3](img/course/20_3.jpg)</p>
   <p align="center">Рис. 55.3. <b>Результат виконання завдання 20 (2)</b></p>
   <br/><br/>

   <p align="center">![Рис. 56.1](img/course/21_1.jpg)</p>
   <p align="center">Рис. 56.1. <b>Умова завдання 21</b></p>
   <br/><br/>
   <p align="center">![Рис. 56.2](img/solutions/21.jpg)</p>
   <p align="center">Рис. 56.2. <b>Результат виконання завдання 21 (1)</b></p>
   <br/><br/>
   <p align="center">![Рис. 56.3](img/course/21_3.jpg)</p>
   <p align="center">Рис. 56.3. <b>Результат виконання завдання 21 (2)</b></p>
   <br/><br/>

   <p align="center">![Рис. 57.1](img/course/22_1.jpg)</p>
   <p align="center">Рис. 57.1. <b>Умова завдання 22</b></p>
   <br/><br/>
   <p align="center">![Рис. 57.2](img/solutions/22.jpg)</p>
   <p align="center">Рис. 57.2. <b>Результат виконання завдання 22 (1)</b></p>
   <br/><br/>
   <p align="center">![Рис. 57.3](img/course/22_3.jpg)</p>
   <p align="center">Рис. 57.3. <b>Результат виконання завдання 22 (2)</b></p>
   <br/><br/>

   <p align="center">![Рис. 58.1](img/course/23_1.jpg)</p>
   <p align="center">Рис. 58.1. <b>Умова завдання 23</b></p>
   <br/><br/>
   <p align="center">![Рис. 58.2](img/solutions/23.jpg)</p>
   <p align="center">Рис. 58.2. <b>Результат виконання завдання 23 (1)</b></p>
   <br/><br/>
   <p align="center">![Рис. 58.3](img/course/23_3.jpg)</p>
   <p align="center">Рис. 58.3. <b>Результат виконання завдання 23 (2)</b></p>
   <br/><br/>

   <p align="center">![Рис. 59.1](img/course/24_1.jpg)</p>
   <p align="center">Рис. 59.1. <b>Умова завдання 24</b></p>
   <br/><br/>
   <p align="center">![Рис. 59.2](img/solutions/24.jpg)</p>
   <p align="center">Рис. 59.2. <b>Результат виконання завдання 24 (1)</b></p>
   <br/><br/>
   <p align="center">![Рис. 59.3](img/course/24_3.jpg)</p>
   <p align="center">Рис. 59.3. <b>Результат виконання завдання 24 (2)</b></p>
   <br/><br/>

   <p align="center">![Рис. 60.1](img/course/25_1.jpg)</p>
   <p align="center">Рис. 60.1. <b>Умова завдання 25</b></p>
   <br/><br/>
   <p align="center">![Рис. 60.2](img/solutions/25.jpg)</p>
   <p align="center">Рис. 60.2. <b>Результат виконання завдання 25 (1)</b></p>
   <br/><br/>
   <p align="center">![Рис. 60.3](img/course/25_3.jpg)</p>
   <p align="center">Рис. 60.3. <b>Результат виконання завдання 25 (2)</b></p>
   <br/><br/>

   <p align="center">![Рис. 61.1](img/course/26_1.jpg)</p>
   <p align="center">Рис. 61.1. <b>Умова завдання 20</b></p>
   <br/><br/>
   <p align="center">![Рис. 61.2](img/solutions/26.jpg)</p>
   <p align="center">Рис. 61.2. <b>Результат виконання завдання 26 (1)</b></p>
   <br/><br/>
   <p align="center">![Рис. 61.3](img/course/26_3.jpg)</p>
   <p align="center">Рис. 61.3. <b>Результат виконання завдання 26 (2)</b></p>
   <br/><br/>

   <p align="center">![Рис. 62.1](img/course/27_1.jpg)</p>
   <p align="center">Рис. 62.1. <b>Умова завдання 27</b></p>
   <br/><br/>
   <p align="center">![Рис. 62.2](img/solutions/27.jpg)</p>
   <p align="center">Рис. 62.2. <b>Результат виконання завдання 27 (1)</b></p>
   <br/><br/>
   <p align="center">![Рис. 62.3](img/course/27_3.jpg)</p>
   <p align="center">Рис. 62.3. <b>Результат виконання завдання 27 (2)</b></p>
   <br/><br/>

   <p align="center">![Рис. 63.1](img/course/28_1.jpg)</p>
   <p align="center">Рис. 63.1. <b>Умова завдання 28</b></p>
   <br/><br/>
   <p align="center">![Рис. 63.2](img/solutions/28.jpg)</p>
   <p align="center">Рис. 63.2. <b>Результат виконання завдання 28 (1)</b></p>
   <br/><br/>
   <p align="center">![Рис. 63.3](img/course/28_3.jpg)</p>
   <p align="center">Рис. 63.3. <b>Результат виконання завдання 28 (2)</b></p>
   <br/><br/>

   <p align="center">![Рис. 64.1](img/course/29_1.jpg)</p>
   <p align="center">Рис. 64.1. <b>Умова завдання 29</b></p>
   <br/><br/>
   <p align="center">![Рис. 64.2](img/solutions/29.jpg)</p>
   <p align="center">Рис. 64.2. <b>Результат виконання завдання 29 (1)</b></p>
   <br/><br/>
   <p align="center">![Рис. 64.3](img/course/29_3.jpg)</p>
   <p align="center">Рис. 64.3. <b>Результат виконання завдання 29 (2)</b></p>
   <br/><br/>

   <p align="center">![Рис. 65.1](img/course/30_1.jpg)</p>
   <p align="center">Рис. 65.1. <b>Умова завдання 30</b></p>
   <br/><br/>
   <p align="center">![Рис. 65.2](img/solutions/30.jpg)</p>
   <p align="center">Рис. 65.2. <b>Результат виконання завдання 30 (1)</b></p>
   <br/><br/>
   <p align="center">![Рис. 65.3](img/course/30_3.jpg)</p>
   <p align="center">Рис. 65.3. <b>Результат виконання завдання 30 (2)</b></p>
   <br/><br/>

   <p align="center">![Рис. 66.1](img/course/31_1.jpg)</p>
   <p align="center">Рис. 66.1. <b>Умова завдання 31</b></p>
   <br/><br/>
   <p align="center">![Рис. 66.2](img/solutions/31.jpg)</p>
   <p align="center">Рис. 66.2. <b>Результат виконання завдання 31 (1)</b></p>
   <br/><br/>
   <p align="center">![Рис. 66.3](img/course/31_3.jpg)</p>
   <p align="center">Рис. 66.3. <b>Результат виконання завдання 31 (2)</b></p>
   <br/><br/>

   <p align="center">![Рис. 67.1](img/course/32_1.jpg)</p>
   <p align="center">Рис. 67.1. <b>Умова завдання 32</b></p>
   <br/><br/>
   <p align="center">![Рис. 67.2](img/solutions/32.jpg)</p>
   <p align="center">Рис. 67.2. <b>Результат виконання завдання 32</b></p>
   <br/><br/>

   <p align="center">![Рис. 68.1](img/course/33_1.jpg)</p>
   <p align="center">Рис. 68.1. <b>Умова завдання 33</b></p>
   <br/><br/>
   <p align="center">![Рис. 68.2](img/solutions/33.jpg)</p>
   <p align="center">Рис. 68.2. <b>Результат виконання завдання 33</b></p>
   <br/><br/>

   <p align="center">![Рис. 69.1](img/course/34_1.jpg)</p>
   <p align="center">Рис. 69.1. <b>Умова завдання 34</b></p>
   <br/><br/>
   <p align="center">![Рис. 69.2](img/solutions/34.jpg)</p>
   <p align="center">Рис. 69.2. <b>Результат виконання завдання 34</b></p>
   <br/><br/>

## Висновки

<p>За результатом виконання лабораторної роботи я навчився користуватися основним функціоналом системи контролю версій GIT, що є однією з найважливіших навичок для будь-якого розробника. Було опрацьовано управління локальним репозитарієм, його взаємодія з віддаленим репозитарієм, а також деякі аспекти командного використання GIT. Отже, я навчився налагоджувати своє оточення як Java-розробника.</p>
